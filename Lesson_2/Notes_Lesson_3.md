# Part 0: Why AWS?
    * put ML in the hands of every developer
    * many versatile, flexible features and cloud services

## State of ML
    * went from aspirational technology to mainstream really fast (a lot of money is spent on ML)
    * 75% of companies will have ML in deployment by 2024
    * boosted by GPUs and TPUs, ML handles real-world tasks
    * ML becomes/is part of the "DevOps" process

## AWS ML offerings/services
* pretrained AI services can be integrated/directly applied
    - **Health AI**: transcribe medical (automatic speech recognition [ASR] for healthcare background)
    - **Industrial AI**: Amazon Monitron: predict machine failures (cost saving/efficiency)
    - **Anomaly detection**: Amazon Lookout for Metric: automatically detect/diagnose anomalies in businesses/operational data
    - **Chatbots**: Amazon Lex helps to publish voice/text bots on mobile devices
    - **Personalization**: Amazon Personalize is service for ML-backed recommendation system for highly customized recommendation across different industries (retail, media, entertainment,...)
    - **Forecast**: Amazon Forecast enables high-accuracy forecasts of product demand, resource needs, or financial performance.
    - **Fraud detection**: Amazon Fraud Detector is an ML-backed fraud detection toolkit       
    - **Code quality**: Amazon Code Guru is a recommendation system that helps optimization and code quality improvement.
    - **Vision**: Amazon Rekognition is a tool for fast and accurate face search for face recognition
    - **Speech**: Amazon Polly turns text into life-like speech
    - **Text**: Amazon Textract can extract text from  scanned documents or images
    - **Contact centers**: Amazon Contact Lens helps to analyze/understand conversations between customers and agents (speech transcription, natural language processing, intelligence search, sentiment analysis, categorize contacts)
    - **Search**: Amazon Kendra can search for your answers in unstructured data sets.

* ML on AWS: build, train, and deploy models quickly
    - challenges may arise at every step in the ML development process:         
            1. data labeling 
            2. collecting data
            3. storing features 
            4. checking for bias 
            5. visualization
            6. selecting an algorithm 
            7. training models 
            8. tuning [hyper-]parameters 
            9. model deployment 
            10. managing and monitoring models 
            11. continuous delivery
    - AWS has offers to deal with these challenges
    - Amazon Sagemaker: remove complexity from each step in the ML workflow to build/train/deploy ML models faster
        - Sagemaker Studio: fully integrated development env. to build/train/deploy ML models
        - Sagemaker distributed training: automatically partition model and data to boost development/training of complicated models
        - Sagemaker Clarify: detect bias in ML models and help interpretability/transparency of models

* ML infrastructure and frameworks on AWS:
    - many useful workflow services (Sagemaker, Deep Learning Containers, AWS Batch, AWS ParallelCluster, Elastic Kubernetes & Container Service, Amazon EMR)
    - support for all leading ML frameworks (TF, Keras, PyTorch, mxnet, GLUON,...)
    - compute, networking, and storage as a service: EC2, Elastic Inference, AWS Outposts, Elastic Fabric Adapter, Amazon S3, Amazon EB5,...

* Getting started with ML with Amazon-sold devices:
    - AWS DeepLens: computer vision with deep-learning-enabled video camera
    - AWS DeepRacer: reinforcement learning with a fully autonnomous race car
    - AWS DeepComposer: generative AI to compose music


## Suggested further reading:
* information about AWS ML services [link](https://aws.amazon.com/de/machine-learning/ai-services/?utm_source=Udacity&utm_medium=Webpage&utm_campaign=Udacity%20AWS%20ML%20Foundations%20Course)
* information about AWS ML training/degrees [link](https://aws.amazon.com/de/machine-learning/ai-services/?utm_source=Udacity&utm_medium=Webpage&utm_campaign=Udacity%20AWS%20ML%20Foundations%20Course)
    

# Part 1: Outline  
* **Part 0:** ML services on AWS
* **Part 2:** Computer vision project 
* **Part 3:** Reinforcement learning with AWS deep racer
* **Part 4:** Generative AI on AWS deep composer

* **Learning goals:**
    - indentify what AWS ML service could be used for an application
    - basics about computer vision ML tasks
    - basics about reinforcement learning 
    - basics about generative AI and famous generative AI applications


# Part 2: Computer vision project



