# Part 1: Outline:

* ### What's ML?
* ### Steps in ML?
* ### Examples (real-world problems)

## Learning Objectives

* discriminate supervised and unsupervised learning
* identify problems that can be solved with ML
* describe common approaches: 
    - linear regression
    - logistic regression 
    - k-means
* describe model testing and training
* evaluate performance of ML models with metrics

# Part 2: What is ML ?

* **AI**: capability of machine to act intelligently


* **ML**: learn from data, extract patterns and infer. Opposed to explicit programming, ML uses training 


* **Sup. L.**: 
    - example-driven learning. 
    - Every data point in the data set has a label and the model learns to predict the labels. 
    - Typical examples: predict sales price of real-estate or classify images

    
* **Unsup. L.**:
    - No labels on the data points in the data set
    - algorithm learns patterns or distributions in the data


* **Reinforcement L.**:
    - Sup. L and Unsup. L. inspect data and infer patterns and humans use these to gain new understanding/make predictions
    - in contrast: R.L. is learning through consequences of actions in an environment
        - similar to training a pet: need to fine-tune rewards and penalty



## How ML helps ?

* ### traditional programming: 
    - human inspects / processes data programming a fixed set of instructions
    - takes a lot of time 
    - may be impossible (too many special cases)
    - example: detecting a cat in an image [(see this notebook)](./Lesson2_Cats.ipynb)
        - need special case for different lighting conditions, color, surroundings, orientations, ...


* ### ML takes a different approach:
    - the computer runs a training algorithm on some input data
    - trained model is the outcome of the training
    - trained model replaces program in traditional programming
    - key advantages: 
        - flexibility (no need for all these special cases) 
        - can make predictions on data that was not in the dataset (i.e., the model generalizes to new data) 


* ### ML Terminology
    - definition of terms still not fully settled, evolving field
    - ML is at the crossroads of 
        - applied math
        - statistics
        - computer science

# Part 3: Components of ML

* ### Getting started

    * **Intro**
        - ML tasks involve three components
            1. ML model
            2. model training algorithm
            3. model inference algorithm
        
        - analogy: teapot from clay
            1. model is like raw clay
            2. training is like forming the teapot from clay
            3. inference is like having tea from the teapot, i.e., using it
    * **Getting started: Models**
        * a model is a generic program made specific only by the training data (like a piece of raw clay, that can become a teapot only by shaping it so)
        * formally, a model is a block of code that can solve different, but related problems
        * examples: 
            - linear regression of the number of snow cones sold as a function of the temperature [(see this sklearn example)](./sklearn_linear_regression_example.ipynb)
            - number of people attending college as a function of the tuition fee

    * **Getting started: Training**
        * many different training algorithms exist
        * iterative recipe:
            - determine the changes required (how to make the clay look more like the teapot I want)        
                - model training algorithm processes data with the model and compares the results against a goal
            - make changes to the model (shape the clay into the teapot gradually)
                - training algorithm gradually changes model parameters to bring model closer to achieving the goal
        * if a termination criterion of iteration is reached, our model is considered trained --> time for inference 
    
    * **Getting started: Inference**
        * inference algorithm uses the trained model to make predictions with new data (our teapot is completed and inspected, time to have tea with it)
        
# Part 4: Introduction to the five ML steps

## The (common) 5 ML steps

This lesson is about getting an idea of the five **iterative** common basic steps with ML.

* ### **Step 1**: definition of the problem
* ### **Step 2**: building the dataset
* ### **Step 3**: training the model
* ### **Step 4**: evaluating the model
* ### **Step 5**: using the model

## Step 1: Defining the problem

* Be specific! Example (ice cream store): Does adding a 50ct fee on mixing different flavors increase the number of snow cones sold? 
Counter example: How can I increase sales? Problems: too broad, not specific.
* Identify ML task
    - clear idea of task will help to design dataset
    - ML tasks:
        - all tasks have in common a dataset, a model, and a model training algorithm
        - outputs of model can be very different
        - labeled data --> supervised learning
        - unlabeled data --> unsupervised learning

### Supervised Learning
* Labeled dataset has data points that already contain the answers to the task we're trying to solve
    - Example: linear regression of the number of snow cones sold as a function of temperature. 
        - the prediction of the model is the expected number of snow cones sold at a certain temperature
        - the **dataset** contained the **number of sales** with the **temperature as label**
        - in the training of the linear regression model, we use the number of sales (data) and the temperature (labels) to get the best linear fit 

* Narrowing down the task with supervised ML / labeled datasets
    * **Classification**: when the data points in the dataset contain labels that indicate a category, i.e., the **labels are categorical**. 
    - Example: ML model that predicts the type of a flower based based on an image of it
    * **Regression**: when the data points in the dataset contain labels which are real numbers, i.e. the **labels are continuous**.


### Unsupervised Learning
* Unlabeled dataset has data points where no labels are given, i.e., we have no example solutions to the task that we're trying to solve
    - Example: picture of a palm tree and the question "What do you see?" The answer "a palm tree" corresponds to labeling the data point/set
    - Example: finding what genre a book is belonging to. Since we can't read all books, we cannot know all genres. 

* Narrowing down the task with unsupervised ML / unlabeled dataset    
    - Common approach: use **clustering** to find groups (genres) in the unlabeled data points (books)

## Step 2: Build the dataset

* working and understanding the dataset
    - collection
    - inspection
    - summary statistics
    - visualization

### Data collection
* need data that is related and relevant for my problem
* Example: detect if a cat is in an image. Internet search could provide labeled and unlabeled data (determining whether a supervised or unsupervised ML task is going to be solved)

### Data inspection
* explore dataset
    - check integrity
        - outliers
        - incomplete data points
    - transform dataset
        - ML typically needs numerical data
        - Ask the question if any of the data needs transformation to be compatible with the model type which you're working on?

### Summary statistics and data visualizations
* can help to better understand data and make the data better-suited for your ML task
* **statistics** (mean, inner quartal range, std. dev.) help
    - to identify trends
    - to identify the scale
    - to understand the shape
* **visualizations** help 
    - to find outliers
    - understand data
    - communicate findings to project stakeholders

[suggested reading: sklearn examples for outlier detection with a real dataset](https://scikit-learn.org/stable/auto_examples/applications/plot_outlier_detection_wine.html?highlight=outlier)


## Step 3: training the model
* split dataset randomly  
    - training dataset (majority, ~80%) used to fit the model with the training algorithm
    - test dataset used for model evaluation that is not shown to the model during training
    - outside the scope of this lesson: validation dataset

### What *is* training a model?
* The model training algorithm iteratively updates the **model parameters** to minimize a **loss function**
    - model parameters are configure how the model behaves (specific terms: weights, biases)
    - example with the linear regression on the number of snow cones sold at a given temperature:       
        - model parameters are the slope and the offset of the linear fit
        - loss function could be the difference between the actual/recorded number of snow cones sold in the training set from the fit line, i.e., the model's prediction for the number of sold snow cones
* in other words: the model training algorithm gradually moves model parameters to move the model closer and closer to achieve some goal which is measured my the loss function
* in practice, one typically resorts to a loss function chosen from a predifined common set (mean-squared error, mean absolute error, ...)

### ... in summary:
**Training a model** means:
1. Feed the training data into the model.
2. Compute the loss function value on the model output.
3. Update the model parameters in a direction that reduces the loss.

### A few other details:
* How to actually implement the model training?
    - can do it from scratch, but only for a completely novel architecture
    - commonly: use ML frameworks with training algorithm implementation
* How to determine which model to use?
    - needs experience, even experts try many different models
* Training algorithm hyperparameters
    - knobs on the model or training algorithm that can not be changed during the training
    - determine reliability and speed of training process
* ML is an iterative process: 
    - assumptions about data need to be questioned
    - assumptions about the problem need to updated
    - try new things and measure success across different iterations

### Extended Learning:
Common ML models include:

* **Linear models**
    - for two-dimensional data (x,y) a linear model corresponds to a function *y = mx + b* . 
    - Classifications often use a strongly related logistic model that adds a transformation to the linear model that maps its output, *y*, into the range *[0,1]*, i.e., the probability to be in a specific class.
    - Try linear models as the most simple starting point
* **Tree-based models**
    - model for classification tasks that builds a very large structure of *if/else* blocks
    - off-the-shelf implementation in sklearn: **XGBoost**
    - try tree models before moving on to more complex ML models
* **Deep learning models**
    - modern approach using artificial neural networks, powerful and popular
    - interconnected neurons connected by weights
    - incomplete list of architectures:
        - FFNN: feed-forward neural network. Stack of layers of neurons with each neuron connected to all neurons in the subsequent (and previous) layer.
        - CNN: convolutional neural networks. Sets of nested filters which can be applied to grid-organized data. Most common in image processing.
        - RNN/LSTM: recurrent neural network or long-short-term memory: structured like loops in traditional computing, good at processing sequences of data.
        - Transformer: modern implementation/replacement of RNN/LSTMs. Tranformer architectures allow training with larger datasets than RNNs/LSTMs

### Machine Learning libraries in Python:
* classical (linear, tree-based) models: **scikit-learn**
* deep learning models: **mxnet**, **tensorflow**, **pytorch**

## Step 4: Model evaluation

* Commonly, model accuracy is a common metric for evaluating the performance of a model.
    - Example distinguish / classify flowers based on measurable details like the length of a leaf. The model accuracy -- i.e., fraction of the predictions that are the correct species of flower -- could then be used.
    - Many metrics exist: 
        - recall $r = \frac{\#tp}{\#tp + \#fn}$ , where $\#tp$ and $\#fn$ is the number of true positives and false negatives, respectively
        - precision $p = \frac{\#tp }{ \# tp + \#fp}$, where $\#tp$ and $\#fp$ is the number of true and false positives, respectively
        - log-loss (calculate how "uncertain" your model is about its predictions): $L_{\log}(y, p) = -(y \log (p) + (1 - y) \log (1 - p))$, here, $y\in \{0,1\}$ is the true label and $p = P(y=1)$ is the probability estimate of the label being $y=1$.
        - mean absolute error (regression) $L = \frac{\sum_{j=1}^N \vert y_j - \hat{y}_j \vert }{N}$
        - hinge loss for binary classification: $l(\hat{y}) = max(0,1-t\hat{y})$, where $l$ is the loss, $\hat{y}$ is the predicted label and $t$ is the true label.
        - F1 score $F_1 =  \frac{ 2 p r} {p + r}$, where $p$ is the precision and $r$ is the recall.
        - R^2 loss (regression): $R^2(y, \hat{y}) = 1 - \frac{\sum_{i=1}^{n} (y_i - \hat{y}_i)^2}{\sum_{i=1}^{n} (y_i - \bar{y})^2}$
        - Quantile Loss (? sklearn's DummyClassifier ?)
* Remember, its an iterative process (**Step 4** can affect any of the previous **Steps 1--3**)
* Once you arrived at a model that meets your demands, you can use the model

### Suggested further reading:
* [AWS / Sagemaker model and healthcare dataset and XGBoost](https://aws.amazon.com/de/blogs/machine-learning/create-a-model-for-predicting-orthopedic-pathology-using-amazon-sagemaker/)
* [details about metrics in sklearn](https://scikit-learn.org/stable/modules/model_evaluation.html)

* [ AWS blog on the importance of evalutaion metrics for model performamce](https://aws.amazon.com/de/blogs/machine-learning/making-accurate-energy-consumption-predictions-with-amazon-forecast/)

## Step 5: Model inference
* Model inference is using a trained and evaluated model to solve real problems
* Monitor if the model actually produces the results that are expected 
* Again: also **Step 5** is part of the iterative process and maybe you have to go back to **Step 1 -- 4**, to refine the quality of the results in **Step 5**


# Examples

## Three cases of ML task examples

### Supervised learning
* Use ML to predict house prices based on lot sizes and number of bedrooms

### Unsupervised learning
* find genres from descriptive words in the back-cover descriptions of the books

### Deep neural network
* detection of chemical spills from images of video footage from a lab (beyond the scope of the course)

## Example 1: Supervised learning of house prices

* **Step 1:** Defining the problem
    - assumption: a relationship between the lot size and the number of bedrooms and the house prize exists
    - choose lot size and number of bedrooms as features for an ML model to predict the label, the house prize
    - since we have some information on the house prizes (real estate agents, ads,...) we can generate a labeled dataset, therefore, this is a *supervised learning task*
    - house price is a continuous label, i.e., a real number. Therefore, our ML task is a *regression* .
* **Step 2:** Dataset generation
    - Data collection: get as much data on house prizes from the neighborhood and pay an appraiser to get an estimate of unknown house prices
    - Data exploration: all house prices, bedroom numbers, and lot sizes are numbers which is good for ML models. Next data needs to be cleaned (find and delete/fill missing data points and drop outliers [example: a house with 10 bedrooms])
    - Data visualization: plot features vs. each label to get an intuition for the underlying relation
    - Dataset preparation: split data points 80/20 into training/validation sets
* **Step 3:** Training the model
    - Connection between features (lot size, number of bedrooms) and labels (house price) seems pretty straightforward
    - try *linear model* (line for one feature, plane for two, hyperplane for more than two)
    - training is done using python to load data into arrays and use toolkit to train classical models
* **Step 4:**  Evaluate the model 
    - Choose a metric for the evaluation: the root-mean-squared error of the model predictions $RMS= \frac{\sum_{j=1}^N \vert y_j - \hat{y}_j \vert^2 }{N}$
    - To determine the accuracy, we can count how often the model prediction error is below a certain threshold
* **Step 5:** Model inference
    - If the training and evaluation was successful and we're now satisfied with the achieved accuracies of house price predictions
    - Use the model to predict house prices for other houses on the market

### Suggested further reading:
* tutorial on Ridge regression models (ordinary least squares with an L2 regularization [cf. [link](https://en.wikipedia.org/wiki/Tikhonov_regularization)], see [this notebook](./Lesson2_RidgeRegression_HousePrices.ipynb) (actually house prices) [link](https://machinelearningmastery.com/ridge-regression-with-python/)

* tutorial on model evaluation metrics for regressions [link](https://machinelearningmastery.com/regression-metrics-for-machine-learning/)


## Example 2: Book Genre Exploration

* Plot: want to write an article discussing the largest book trends of the year.
* Tasks: 
    - identify trends 
    - identify micro-genres (teen vampire romance novels)
* Intuition: 
    - Backcover text contains information that allows one to identify the micro-genre 
    - ML is suited to achieve this task
    
* **Step 1:** Define the problem
    - books with similar content have similar descriptions
    - assumption: micro-genres can be identified as clusters whose descriptions use very similar words
    - problem: find clusters of similar books based on the presence of certain words
    - this is an *unsupervised learning* task, because the data is unlabeled
    - use the *clustering approach*, as we assume the problem is a clustering problem

* **Step 2:** Data collection
    - collect book backcover texts from 800 romance books of the previous year
    - clean data (remove unimportant features for the ML task):
        - remove capitals 
        - convert all verbs to the same tense 
        - remove punctuation
        - remove unimportant words (a, the, ...)
    - preprocessing: 
        - data vectorization (text --> "bag of words" (vectors of integers))

* **Step 3:** Train the model
    - choose simple model: $k$-means
        - the $k$ stands for the number of clusters
        - since we don't know how many micro-genres/clusters there are, we will train the model for different values of $k$
        - for the evaluation step, we'll have to keep the $k$ in mind

* **Step 4:** Evaluate the model
    - many metrics exist for evaluating model performance 
        - Fowlkes-Mallows
        - V-measure
        - Homogeneity
        - Calinsky-Harabasz Index
        - Silhouette coefficient
        - Completeness
        - Pair confusion matrix
        - Contingency Matrix
        - Rand index
        - Mutual information
        - Davis-Bouldin index
    - here, we use the Silhouette coefficient to measure the quality of the clustering of our data
    - plot metric as a function of the number of clusters $k$
    - from some $k$, the metric (Silhouette coefficient) doesn't improve any more
    - perform manual inspection to assign labels like "paranormal teen romance"

* **Step 5:** Using the model
    - check if one (some) of the clusters identified is (are) interesting enough to write your article
    - cluster 7: books about long-distance relationships



## Example 3: Spill Detection from Video

* Plot: janitor service for chemical plant client that needs fast response time. 
* Tasks: 
    - detect spills from plant surveillance system
* Intuition: 
    - machine learning can be used to automate the detection of spills

* **Step 1:** Define the problem
    - supervised classification tasks on images that classifies the presence or absence of spills



* **Step 2:** Data collection
    - collect images and label them with "spill" and "no spill"
    - explore and clean (different lighting conditions, spill clearly in the image, ...)
    - convert to numerical representation
    - split into training and test set

* **Step 3:** Train the model
    - convolutional neural networks (CNNs) are good at feature detection
    - CNNs are made up of layers with different filters (details go beyond the scope of this lesson)
    - model is trained to learn classification of features

* **Step 4:** Evaluate the model
    - many metrics exist for evaluating model performance:
        - Accuracy
        - Precision
        - Confusion matrix
        - ROC curve
        - Recall
        - False positive rate
        - False negative rate
        - Log loss
        - Specifity
        - F1 Score
    - precision and recall will be effective in the present example
        - accuracy (percentage of correct predictions) isn't so good, because most of the time there's no spill (predicting 'no spill' all the time is pretty accurate...)
        - metric/evaluation needs to select models that rarely miss a real spill:
            - Precision: How many of all the predictions of a spill were right? In other words: How often a janitor is sent to clean a real spill?
            - Recall: Of all actual spills, how many have we detected? In other words: How many of the spills did my system catch?
        - perform manual testing
            - spills in dataset might not have been sufficiently realistic
            - find some additional data from the surveillance system and test the model on it
    

* **Step 5:** Using the model
    - deploy model to automatically process data from surveillance system of the plant
    - trigger paging of janitor when spill is detected
       




